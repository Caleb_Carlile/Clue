package edu.neumont.csc110.a.finalproject;

import java.util.Random;

public class EditWeapon {
	public static final String[] names = new String[] { "Candlestick", "Lead Pipe", "Knife", "Revolver", "Rope", "Wrench" };

	public static void makeWeapons() {
		for (int i = 0; i < 6; i++) {
			Game.Weapons.add(new Weapon());
			Game.Weapons.get(i).name = names[i];
		}
	}

	public static void chooseMurderWeapon() {
		Random rng = new Random();
		int i = rng.nextInt(6) + 1;
		int j = 0;
		for (Weapon WeaponsIndex : Game.Weapons) {
			j++;
			if (j == i) {
				WeaponsIndex.isMurderWeapon = true;
			}
		}
	}

}
