package edu.neumont.csc110.a.finalproject;

public enum Detective {
	Professor_Plum("Professor Plum"), Colonel_Mustard("Colonel Mustard"), Mr_Green("Mr. Green"), Mrs_Peacock(
			"Mrs. Peacock"), Miss_Scarlet("Miss_Scarlet"), Mrs_White("Mrs. White");

	private String name;

	Detective(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}
