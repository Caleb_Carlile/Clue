package edu.neumont.csc110.a.finalproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleUI {

	public static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

	/**
	 * Generates a console-based menu using the Strings in options as the menu
	 * items. Reserves the number 0 for the "quit" option when withQuit is true.
	 * 
	 * @param options
	 *            - Strings representing the menu options
	 * @param withQuit
	 *            - adds option 0 for "quit" when true
	 * @return the int of the selection made by the user
	 * @throws IOException
	 */
	static int promptForMenuSelection(String[] options, boolean withQuit) throws IOException {
		int arrayLen = options.length;
		boolean goodInput = false;
		int input;
		int q;
		if (!withQuit) {
			q = 1;
		} else {
			q = 0;
		}
		do {
			System.out.println("Please choose from the list of options.");
			for (int i = q; i < arrayLen; i++) {
				System.out.println(i + ": " + options[i]);
			}
			try {
				String stringInput = in.readLine();
				input = Integer.parseInt(stringInput);
			} catch (NumberFormatException numException) {
				System.out.println("Input must be numeric. Type an options menu number.");
				input = -1;
			}
			if (input >= q && input < arrayLen) {
				goodInput = true;
			} else {
				System.out.println("Input is invalid.");
				goodInput = false;
			}

		} while (!goodInput);
		return input;
	}

	/**
	 * Generates a prompt that expects the user to enter one of two responses
	 * that will equate to a boolean value. The trueString represents the case
	 * insensitive response that will equate to true. The falseString acts
	 * similarly, but for a false boolean value. Example: Assume this method is
	 * called with a trueString argument of "yes" and a falseString argument of
	 * "no". If the enters "YES", the method returns true. If the user enters
	 * "no", the method returns false. All other inputs are considered invalid,
	 * the user will be informed, and the prompt will repeat.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param trueString
	 *            - the case insensitive value that will evaluate to true
	 * @param falseString
	 *            - the case insensitive value that will evaluate to false
	 * @return the boolean value
	 * @throws IOException
	 */
	static boolean promptForBool(String prompt, String trueString, String falseString) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		String stringInput = null;
		boolean input = false;
		boolean goodInput = false;
		do {
			System.out.println(prompt);
			stringInput = in.readLine();
			if (stringInput.equalsIgnoreCase(trueString)) {
				input = true;
				goodInput = true;
			} else if (stringInput.equalsIgnoreCase(falseString)) {
				input = false;
				goodInput = true;
			} else {
				System.out.println("Invalid input.");
			}
		} while (!goodInput);

		return input;
	}

	/**
	 * Generates a prompt that expects a numeric input representing a byte
	 * value. This method loops until valid input is given.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param min
	 *            - the inclusive minimum boundary
	 * @param max
	 *            - the inclusive maximum boundary
	 * @return the byte value
	 * @throws IOException
	 */
	static byte promptForByte(String prompt, byte min, byte max) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		String inputString;
		byte input = 0;
		boolean goodInput = false;
		System.out.println(prompt);
		do {
			try {
				inputString = in.readLine();
				input = Byte.parseByte(inputString);

			} catch (NumberFormatException numberException) {
				System.out.println("Input must be numeric.");
				input = (byte) (min - 1);

			}

			if (input >= min && input <= max) {
				goodInput = true;
			} else {
				System.out.println("Input must be between " + min + " & " + max + ".");
				goodInput = false;
			}

		} while (!goodInput);

		return input;
	}

	/**
	 * Generates a prompt that expects a numeric input representing a short
	 * value. This method loops until valid input is given.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param min
	 *            - the inclusive minimum boundary
	 * @param max
	 *            - the inclusive maximum boundary
	 * @return the short value
	 * @throws IOException
	 */
	static short promptForShort(String prompt, short min, short max) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		String inputString;
		short input = 0;
		boolean goodInput = false;
		System.out.println(prompt);
		do {
			try {
				inputString = in.readLine();
				input = Short.parseShort(inputString);

			} catch (NumberFormatException numberException) {
				System.out.println("Input must be numeric.");
				input = (short) (min - 1);

			}

			if (input >= min && input <= max) {
				goodInput = true;
			} else {
				System.out.println("Input must be between " + min + " & " + max + ".");
				goodInput = false;
			}

		} while (!goodInput);

		return input;
	}

	/**
	 * Generates a prompt that expects a numeric input representing an int
	 * value. This method loops until valid input is given.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param min
	 *            - the inclusive minimum boundary
	 * @param max
	 *            - the inclusive maximum boundary
	 * @return the int value
	 * @throws IOException
	 */
	static int promptForInt(String prompt, int min, int max) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		String inputString;
		int input = 0;
		boolean goodInput = false;
		System.out.println(prompt);
		do {
			try {
				inputString = in.readLine();
				input = Integer.parseInt(inputString);

			} catch (NumberFormatException numberException) {
				System.out.println("Input must be numeric.");
				input = min - 1;

			}

			if (input >= min && input <= max) {
				goodInput = true;
			} else {
				System.out.println("Input must be between " + min + " & " + max + ".");
				goodInput = false;
			}

		} while (!goodInput);

		return input;
	}

	/**
	 * Generates a prompt that expects a numeric input representing a long
	 * value. This method loops until valid input is given.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param min
	 *            - the inclusive minimum boundary
	 * @param max
	 *            - the inclusive maximum boundary
	 * @return the long value
	 * @throws IOException
	 */
	static long promptForLong(String prompt, long min, long max) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		String inputString;
		long input = 0;
		boolean goodInput = false;
		System.out.println(prompt);
		do {
			try {
				inputString = in.readLine();
				input = Long.parseLong(inputString);

			} catch (NumberFormatException numberException) {
				System.out.println("Input must be numeric.");
				input = min - 1;

			}

			if (input >= min && input <= max) {
				goodInput = true;
			} else {
				System.out.println("Input must be between " + min + " & " + max + ".");
				goodInput = false;
			}

		} while (!goodInput);

		return input;
	}

	/**
	 * Generates a prompt that expects a numeric input representing a float
	 * value. This method loops until valid input is given.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param min
	 *            - the inclusive minimum boundary
	 * @param max
	 *            - the inclusive maximum boundary
	 * @return the float value
	 * @throws IOException
	 */
	static float promptForFloat(String prompt, float min, float max) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		String inputString;
		float input = 0;
		boolean goodInput = false;
		System.out.println(prompt);
		do {
			try {
				inputString = in.readLine();
				input = Float.parseFloat(inputString);

			} catch (NumberFormatException numberException) {
				System.out.println("Input must be numeric.");
				input = min - 1;

			}

			if (input >= min && input <= max) {
				goodInput = true;
			} else {
				System.out.println("Input must be between " + min + " & " + max + ".");
				goodInput = false;
			}

		} while (!goodInput);

		return input;
	}

	/**
	 * Generates a prompt that expects a numeric input representing a double
	 * value. This method loops until valid input is given.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param min
	 *            - the inclusive minimum boundary
	 * @param max
	 *            - the inclusive maximum boundary
	 * @return the double value
	 * @throws IOException
	 */
	static double promptForDouble(String prompt, double min, double max) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		String inputString;
		double input = 0;
		boolean goodInput = false;
		System.out.println(prompt);
		do {
			try {
				inputString = in.readLine();
				input = Double.parseDouble(inputString);

			} catch (NumberFormatException numberException) {
				System.out.println("Input must be numeric.");
				input = min - 1;

			}

			if (input >= min && input <= max) {
				goodInput = true;
			} else {
				System.out.println("Input must be between " + min + " & " + max + ".");
				goodInput = false;
			}

		} while (!goodInput);

		return input;
	}

	/**
	 * Generates a prompt that allows the user to enter any response and returns
	 * the String. When allowEmpty is true, empty responses are valid. When
	 * false, responses must contain at least one character (including
	 * whitespace).
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user.
	 * @param allowEmpty
	 *            - when true, makes empty responses valid
	 * @return the input from the user as a String
	 * @throws IOException
	 */
	public static String promptForInput(String prompt, boolean allowEmpty) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		System.out.println(prompt);
		String input = in.readLine();
		while (input.isEmpty() == true && !allowEmpty) {
			System.out.println("Sorry, input is required.");
			System.out.println(prompt);
			input = in.readLine();
		}
		return input;
	}

	/**
	 * Generates a prompt that expects a single character input representing a
	 * char value. This method loops until valid input is given.
	 * 
	 * @param prompt
	 *            - the prompt to be displayed to the user
	 * @param min
	 *            - the inclusive minimum boundary
	 * @param max
	 *            - the inclusive maximum boundary
	 * @return the char value
	 * @throws IOException
	 */
	static char promptForChar(String prompt, char min, char max) throws IOException {
		if (prompt == null) {
			IllegalArgumentException myException = new IllegalArgumentException("Prompt can't equal null");
			throw myException;
		}
		boolean goodInput = false;
		char input;
		do {
			System.out.println(prompt);
			input = (char) (System.in.read());
			if (input >= min && input <= max) {
				goodInput = true;
			} else {
				System.out.println("Input must be between " + min + " & " + max + ".");
			}
		} while (!goodInput);
		return input;
	}
}
