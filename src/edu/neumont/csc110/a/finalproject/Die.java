package edu.neumont.csc110.a.finalproject;

import java.util.Random;

public class Die {
	// D12 Rolls. They have to be higher than 2 and no Greater than 12.
	// Edit: 2 D6 rolls. Generate two random ints between 1 and 6. This keeps
	// probability of low and high numbers correct.
	public int rolledDice() {
		final int COUNT_ROLLS = 2;
		Random generator = new Random();
		int[] rolls = new int[COUNT_ROLLS];
		for (int i = 0; i < COUNT_ROLLS; i++) {
			rolls[i] = generator.nextInt(6) + 1;
		}
		System.out.println("Your roll is: " + sumArrayValues(rolls));
		return sumArrayValues(rolls);
	}

	public static int sumArrayValues(int[] array) {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum = sum + array[i];
		}
		return sum;
	}
}
