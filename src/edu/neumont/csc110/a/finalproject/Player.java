package edu.neumont.csc110.a.finalproject;

import java.awt.Color;
import java.util.ArrayList;

public class Player {
	// Amount of players, 3 - 6. Assigns Player as the murderer and assigns them
	// to a character pawn.
	// Assigns Murderer Player with Room and Weapon as well.
	public String name;
	public boolean isKiller;
	public boolean isPlayer;
	public Position position = new Position();
	public ArrayList<Note> myNotes = new ArrayList<>();
	public ArrayList<Card> myCards = new ArrayList<>();
	public ArrayList<Card> seenCards = new ArrayList<>();
	public Color myColor;
	ArrayList<String> options = new ArrayList<>();
	public String roomOccupied;
	public boolean winner;
	public boolean inGame;
	public boolean wasMoved;

	public void printCards() {
		for (int i = 0; i < myCards.size(); i++) {
			System.out.println(name + " " + myCards.get(i).type + ": " + myCards.get(i).name);
		}
	}

	public void updateSeen() {
		for(Note n : myNotes) {
			for(Card c : myCards) {
				if(c.name.equals(n.name)) {
					n.isSeen = true;
				}
			}
			for(Card c : seenCards) {
				if(c.name.equals(n.name)) {
					n.isSeen = true;
				}
			}
		}
	}
}
