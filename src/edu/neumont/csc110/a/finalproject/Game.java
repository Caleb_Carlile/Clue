package edu.neumont.csc110.a.finalproject;

import java.io.IOException;
import java.util.ArrayList;

public class Game {
	// Where the main code is that ties in all the other classes together,
	// whilst using it's own code
	// to play the game correctly.

	/*
	 * At some point with the accusation, the code should be able to correctly
	 * call the right combination of Murderer (Player who is selected) + Correct
	 * Weapon Used + Correct Room that had the murder based on Player Accusation
	 * Assume that the Murderer will have these assignments
	 */
	public static int playerCount;
	public static ArrayList<Player> Players = new ArrayList<Player>();
	public static ArrayList<Room> Rooms = new ArrayList<Room>();
	public static ArrayList<Weapon> Weapons = new ArrayList<Weapon>();

	public void start() throws IOException, InterruptedException {
		// initialize players
		EditPlayer.initializeDetectives();
		Players = new ArrayList<>();
		Dealer.deck = new ArrayList<>();
		Rooms = new ArrayList<>();
		Weapons = new ArrayList<>();
		EditPlayer.playersJoined();
		EditPlayer.getPlayerName();
		EditPlayer.setStartPosition();
		EditPlayer.initializeNotes();
		EditPlayer.addOptions();
		// initialize rooms
		EditRoom.makeRooms();
		// initialize weapons
		EditWeapon.makeWeapons();
		// initialize deck
		Dealer.makeCards();
		// initialize caseFile
		Dealer.makeCaseFile();
		// Deal Cards
		for (int i = 0; i < Dealer.deck.size() - 3; i = i + 0) {
			for (int j = 0; j < Game.playerCount; i++, j++) {
				if (i == Dealer.deck.size() - 3) {
					break;
				}
				Players.get(j).myCards.add(Dealer.dealCard());
				Players.get(j).myCards.get(Players.get(j).myCards.size() - 1).isOwned = true;
			}

		}
		// initialize board
		Board.makeBoard();
		// initialize detectivePads
		play();

	}

	public void play() throws IOException, InterruptedException {
		Game.clearScreen();
		boolean stillPlaying = true;
		int s = 0;
		boolean scarlet = false;
		for (int i = 0; i < Game.playerCount; i++) {
			if (Players.get(i).name.equals("Miss Scarlet")) {
				scarlet = true;
				s = i;
			}
		}
		while (stillPlaying) {
			for (int i = 0; i < playerCount; i++) {
				if (!stillPlaying) {
					break;
				}
				Player p = Players.get(i);
				boolean stillTurn = true;
				if (scarlet == true && i == 0) {
					p = Players.get(s);
				}
				if (scarlet == true && i == s) {
					p = Players.get(0);
				}
				if (p.inGame == false) {
					stillTurn = false;
				}
				Game.clearScreen();
				p.updateSeen();
				while (stillTurn) {
					boolean cont = false;
					Game.printScreen();
					System.out.println(p.name + "'s turn.");
					boolean suggest = false;
					if (p.wasMoved) {
						suggest = ConsoleUI.promptForBool(
								"You were moved into the " + p.roomOccupied
										+ " Would you like to make a suggestion here?\n" + "Type Y for yes or N for no",
								"Y", "N");
					}
					if (!suggest) {
						String[] options = new String[] { "Use a secret passage", "Roll", "Look at notes",
								"Look at cards", "Print board", "Make an accustation", "Skip your turn" };
						boolean extended = false;
						if (p.roomOccupied.equals("Study") || p.roomOccupied.equals("Kitchen")
								|| p.roomOccupied.equals("Conservatory") || p.roomOccupied.equals("Lounge")) {
							extended = true;
						} else {
							extended = false;
						}
						do {
							int input = ConsoleUI.promptForMenuSelection(options, extended);
							switch (input) {
							case 0:
								cont = true;
								CharacterPawn.secretPassage(p);
								if (!p.roomOccupied.equals("none")) {
									Game.makeSuggestion(p, i, true);
								}
								break;
							case 1:
								cont = true;
								ArrayList<Position> possible = CharacterPawn.getPossibleStartPositions(p);
								Die d = new Die();
								int roll = d.rolledDice();
								for (Position pos : possible) {
									Board.available.addAll(CharacterPawn.getAvailable(pos, p, roll));
								}
								Board.available = CharacterPawn.checkMovedRoom(Board.available, p);
								boolean validList = false;
								for (Position av : Board.available) {
									if (av.x >= 0 && av.y >= 0) {
										validList = true;
									}
								}
								if (!validList) {
									Game.printScreen();
									System.out.println("You are trapped. Try something else, or skip your turn.");
									cont = false;
								} else {
									Game.printScreen();
									CharacterPawn.getMoveChoice(p);
									if (!p.roomOccupied.equals("none")) {
										for (Player oth : Players) {
											if (p.position.x == oth.position.x && p.position.y == oth.position.y) {
												if (!p.name.equals(oth.name)) {
													p.position = CharacterPawn.moveToAvailableSpaceInRoom(p);
												}
											}
										}
									}
									Game.clearScreen();
									Game.printScreen();
									Board.available = new ArrayList<>();
									if (!p.roomOccupied.equals("none")) {
										Game.makeSuggestion(p, i, true);
									}
								}
								break;
							case 2:
								cont = false;
								DetectivePad.padMenu(p);
								break;
							case 3:
								cont = false;
								p.printCards();
								break;
							case 4:
								cont = false;
								Game.printScreen();
								break;
							case 5:
								cont = true;
								stillPlaying = Game.makeAccusation(p);
								stillTurn = false;
								break;
							case 6:
								cont = true;
								break;
							}
						} while (!cont);
					} else {
						Game.makeSuggestion(p, i, false);
					}
					p.wasMoved = false;
					stillTurn = false;
				}
			}

		}

	}

	private static void makeSuggestion(Player p, int i, boolean prompt) throws IOException, InterruptedException {
		boolean suggest = true;
		if (prompt) {
			System.out.println("You are in the " + p.roomOccupied + ".");
			suggest = ConsoleUI.promptForBool(
					"Would you like to make a suggestion in this room? \n" + "Type Y for yes or N for no.", "Y", "N");
		} else {
			suggest = true;
		}

		if (!suggest) {

		} else {
			String currentRoom = p.roomOccupied;
			// Menu for Murderer
			String[] detective = new String[] { "Look at Notes", "Professor Plum", "Colonel Mustard", "Mr. Green",
					"Mrs. Peacock", "Miss Scarlet", "Mrs. White" };
			boolean input = false;
			int select = 0;
			while (!input) {
				System.out.println("Enter your suggestion for the murderer.");
				select = ConsoleUI.promptForMenuSelection(detective, true);
				if (select == 0) {
					DetectivePad.printNotes(p);
				} else {
					input = true;
				}

			}
			Card detectiveGuess = new Card();
			detectiveGuess.name = detective[select];
			for (Player pawn : Game.Players) {
				if (pawn.name.equals(detectiveGuess.name) && pawn.roomOccupied != p.roomOccupied) {
					int s = -1;
					Position r1 = pawn.position;
					for (int l = 0; l < EditRoom.roomNames.length; l++) {
						if (EditRoom.roomNames[l].equals(p.roomOccupied)) {
							s = l;
						}
					}
					switch (s) {
					case 0:
						r1.x = 20;
						r1.y = 4;
						break;
					case 1:
						r1.x = 20;
						r1.y = 7;
						break;
					case 2:
						r1.x = 18;
						r1.y = 15;
						break;
					case 3:
						r1.x = 15;
						r1.y = 5;
						break;
					case 4:
						r1.x = 8;
						r1.y = 4;
						break;
					case 5:
						r1.x = 3;
						r1.y = 4;
						break;
					case 6:
						r1.x = 6;
						r1.y = 9;
						break;
					case 7:
						r1.x = 5;
						r1.y = 17;
						break;
					case 8:
						r1.x = 11;
						r1.y = 14;
						break;
					}
					pawn.position = r1;
					pawn.position = CharacterPawn.moveToAvailableSpaceInRoom(pawn);
					pawn.roomOccupied = p.roomOccupied;
					pawn.wasMoved = true;
				} else {

				}
			}
			input = false;

			// Menu for Weapon
			String[] weapons = new String[] { "Look at Notes", "Candlestick", "Lead Pipe", "Knife", "Revolver", "Rope",
					"Wrench" };
			boolean inputWep = false;
			int selectwep = 0;
			while (!inputWep) {
				System.out.println("Enter you suggestion for the murder weapon.");
				selectwep = ConsoleUI.promptForMenuSelection(weapons, true);
				if (selectwep == 0) {
					DetectivePad.printNotes(p);
				} else {
					inputWep = true;
				}

			}
			Card weaponGuess = new Card();
			weaponGuess.name = weapons[selectwep];
			inputWep = false;

			Game.clearScreen();

			System.out.println(p.name + " has suggested that " + detectiveGuess.name + " committed the murder in the "
					+ currentRoom + " with the " + weaponGuess.name);

			int curPlayer = i;
			boolean shownCard = false;
			for (int j = i + 1; j != curPlayer; j++) {
				if (j > playerCount) {
					j = 0;
					if (j == curPlayer) {
						break;
					}
				}
				for (Card c : Game.Players.get(j).myCards) {
					if ((c.name.equals(weaponGuess.name) && c.isOwned)
							|| (c.name.equals(detectiveGuess.name) && c.isOwned)
							|| (c.name.equals(currentRoom) && c.isOwned)) {
						ArrayList<Card> showOptions = new ArrayList<>();
						for (Card cc : Game.Players.get(j).myCards) {
							if (cc.name.equals(weaponGuess.name) && cc.isOwned == true) {
								showOptions.add(cc);
							}
							if (cc.name.equals(detectiveGuess.name) && cc.isOwned == true) {
								showOptions.add(cc);
							}
							if (cc.name.equals(currentRoom) && cc.isOwned == true) {
								showOptions.add(cc);
							}
						}
						ConsoleUI.promptForInput(Game.Players.get(j).name
								+ ", you can disprove the suggestion. Press Enter to continue.", true);
						String[] options = new String[showOptions.size()];
						for (int k = 0; k < options.length; k++) {
							options[k] = showOptions.get(k).name;
						}
						int selection = ConsoleUI.promptForMenuSelection(options, true);
						Game.clearScreen();
						p.myCards.add(showOptions.get(selection));
						p.myCards.get(p.myCards.size() - 1).isOwned = false;
						p.myCards.get(p.myCards.size() - 1).isSeen = true;
						System.out.println(Game.Players.get(j).name + " has shown you the card "
								+ p.myCards.get(p.myCards.size() - 1).name + ".");
						System.out.println(
								"This card has already been marked for you in your notepad. Would you like to edit your notes?");
						int menuSelection = ConsoleUI
								.promptForMenuSelection(new String[] { "Do Nothing", "Edit your notes" }, true);
						if (menuSelection == 1) {
							DetectivePad.padMenu(p);
						}
						shownCard = true;
						break;
					}
				}

				if (shownCard == true) {
					break;
				}
			}
			if (shownCard == false) {
				ConsoleUI.promptForInput("No one was able to disprove your suggestion", true);
			}
		}
	}

	private static void clearScreen() throws IOException, InterruptedException {
		//clears the console to prevent seeing player sensitive text.
		//cls for windows cmd.
		new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		//Print 50000 lines when using eclipse
		/*
		 * for (int i = 0; i < 50000; i++) { 
		 * System.out.println(); }
		 */
	}

	public static void printScreen() {
		Board.closeWindow();
		Board.printBoard();
	}

	private static boolean makeAccusation(Player p) throws IOException {
		String[] detective = new String[] { "Look at Notes", "Professor Plum", "Colonel Mustard", "Mr. Green",
				"Mrs. Peacock", "Miss Scarlet", "Mrs. White" };
		boolean input = false;
		int select = 0;
		while (!input) {
			select = ConsoleUI.promptForMenuSelection(detective, true);
			if (select == 0) {
				DetectivePad.printNotes(p);
			} else {
				input = true;
			}
		}
		Card detectiveGuess = new Card();
		detectiveGuess.name = detective[select];
		input = false;
		String[] room = new String[] { "Look at notes", "Study", "Hall", "Lounge", "Library", "Billiard Room",
				"Conservatory", "Ballroom", "Kitchen", "Dining Room" };
		while (!input) {
			select = ConsoleUI.promptForMenuSelection(room, true);
			if (select == 0) {
				DetectivePad.printNotes(p);
			} else {
				input = true;
			}
		}
		Card roomGuess = new Card();
		roomGuess.name = room[select];
		input = false;
		String[] weapon = new String[] { "Look at notes ", "Candlestick", "Lead Pipe", "Knife", "Revolver", "Rope",
				"Wrench" };
		while (!input) {
			select = ConsoleUI.promptForMenuSelection(weapon, true);
			if (select == 0) {
				DetectivePad.printNotes(p);
			} else {
				input = true;
			}
		}
		Card weaponGuess = new Card();
		weaponGuess.name = weapon[select];
		if (detectiveGuess.name.equals(CaseFile.Detective.name)) {
			if (roomGuess.name.equals(CaseFile.Room.name)) {
				if (weaponGuess.name.equals(CaseFile.Weapon.name)) {
					System.out.println("Congratulations, " + p.name + ", you won!");
					System.out.println("The Murderer was " + CaseFile.Detective.name + " in the " + CaseFile.Room.name
							+ " with the " + CaseFile.Weapon.name + ".");
					return false;
				}
			}
		}
		p.inGame = false;
		int remaining = 0;
		int playerRemaining = 0;
		for (int i = 0; i < playerCount; i++) {
			Player oth = Game.Players.get(i);
			if (oth.inGame) {
				remaining++;
				playerRemaining = i;
			}
		}
		if (remaining == 1) {
			ConsoleUI.promptForInput("You were incorrect. You have lost, and are no longer in the game.", true);
			System.out.println("Congratulations " + Game.Players.get(playerRemaining).name
					+ ", You are the last player remaining. You won!");
			ConsoleUI.promptForInput("The Murderer was " + CaseFile.Detective.name + ", in the " + CaseFile.Room.name
					+ ", with the " + CaseFile.Weapon.name + ".", true);
			return false;

		} else {
			ConsoleUI.promptForInput(
					"You were incorrect. You have lost, and are no longer in the game. You will still be required to show cards.",
					true);
			p.position.x = -2;
			p.position.y = -2;
			return true;
		}
	}

}
