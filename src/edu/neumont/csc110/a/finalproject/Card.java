package edu.neumont.csc110.a.finalproject;

public class Card {
	public String name;
	public boolean isOwned;
	public boolean isSeen;
	public CardType type;
	public boolean isCaseFile;
}
