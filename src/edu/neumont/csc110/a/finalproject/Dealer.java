package edu.neumont.csc110.a.finalproject;

import java.util.ArrayList;
import java.util.Random;

public class Dealer {

	public static ArrayList<Card> deck = new ArrayList<>();

	public static void makeCards() {
		for (int i = 0; i < Game.Players.size(); i++) {
			deck.add(new Card());
			deck.get(i).name = Game.Players.get(i).name;
			deck.get(i).type = CardType.Detective;
		}
		for (int i = 0; i < Game.Weapons.size(); i++) {
			deck.add(new Card());
			deck.get(i + 6).name = Game.Weapons.get(i).name;
			deck.get(i + 6).type = CardType.Weapon;
		}
		for (int i = 0; i < Game.Rooms.size(); i++) {
			deck.add(new Card());
			deck.get(i + 12).name = Game.Rooms.get(i).name;
			deck.get(i + 12).type = CardType.Room;
		}
	}

	public static void makeCaseFile() {
		CaseFile.Weapon = Dealer.dealCaseFile(CardType.Weapon);
		CaseFile.Room = Dealer.dealCaseFile(CardType.Room);
		CaseFile.Detective = Dealer.dealCaseFile(CardType.Detective);
	}

	public static Card dealCaseFile(CardType type) {
		Random r = new Random();
		boolean validType = false;
		do {
			int cardIndex = r.nextInt(deck.size());
			if (deck.get(cardIndex).type.equals(type)) {
				validType = true;
				deck.get(cardIndex).isCaseFile = true;
				return deck.get(cardIndex);
			}
		} while (!validType);
		return null;
	}

	public static Card dealCard() {
		Random r = new Random();
		boolean validCard = false;
		do {
			int cardIndex = r.nextInt(deck.size());
			if (deck.get(cardIndex).isOwned == true) {
				validCard = false;
			} else if (deck.get(cardIndex).isCaseFile == true) {
				validCard = false;
			} else {
				deck.get(cardIndex).isOwned = true;
				return deck.get(cardIndex);
			}
		} while (!validCard);
		return null;

	}
}
