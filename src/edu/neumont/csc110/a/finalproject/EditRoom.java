package edu.neumont.csc110.a.finalproject;

import java.util.Random;

public class EditRoom {
	public static final String[] roomNames = new String[] { "Study", "Hall", "Lounge", "Library", "Billiard Room",
			"Conservatory", "Ballroom", "Kitchen", "Dining Room" };

	public static void makeRooms() {
		for (int i = 0; i < roomNames.length; i++) {
			Game.Rooms.add(new Room());
			Game.Rooms.get(i).name = roomNames[i];
			if (Game.Rooms.get(i).name.equals("Kitchen") || Game.Rooms.get(i).name.equals("Study")) {
				Game.Rooms.get(i).hasPassage1 = true;
			} else if (Game.Rooms.get(i).name.equals("Lounge") || Game.Rooms.get(i).name.equals("Conservatory")) {
				Game.Rooms.get(i).hasPassage2 = true;
			}
		}

	}

	public static void chooseCrimeScene() {
		Random r = new Random();
		int i = r.nextInt(9) + 1;
		int j = 0;
		for (Room roomIndex : Game.Rooms) {
			j++;
			if (j == i) {
				roomIndex.isCrimeScene = true;
			}

		}

	}

}
