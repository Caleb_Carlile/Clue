package edu.neumont.csc110.a.finalproject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class CharacterPawn {
	// The Pawn for each player, will be placed on the board for placement.
//TODO fix diagonal movement
	public static ArrayList<Position> checked = new ArrayList<>();

	public static ArrayList<Position> getAvailable(Position pos, Player p, int roll) {
		ArrayList<Position> available = new ArrayList<>();
		ArrayList<Position> unchecked = new ArrayList<>();
		checked = new ArrayList<>();
		for (int d = 0; d < 4; d++) {
			Position dp = addAvailable(d, pos.x, pos.y, available, 1);
			boolean occupied = false;
			for (Player oth : Game.Players) {
				if (dp.x == oth.position.x && dp.y == oth.position.y && oth.roomOccupied.equals("none")) {
					occupied = true;
				}
			}
			if (!occupied) {
				available.add(dp);
				unchecked.add(dp);
			}
		}
		for (int i = 0; i < roll - 1; i++) {
			int u = unchecked.size();
			for (int j = 0; j < u; j++) {
				for (int d = 0; d < 4; d++) {
					Position dp = addAvailable(d, unchecked.get(0).x, unchecked.get(0).y, available, 1);
					boolean valid = true;
					if (dp.equals(pos)) {
						valid = false;
					} else if (dp.x == -1) {
						valid = false;
					} else {
						for(Player oth : Game.Players) {
							if(dp.x == oth.position.x && dp.y == oth.position.y && oth.roomOccupied.equals("none")) {
								valid = false;
							}
						}
					}
					if (valid){
						available.add(dp);
						unchecked.add(dp);
					}
				}
				unchecked.remove(0);
			}
		}

		for (Player pl : Game.Players) {
			for (Position av : available) {
				if (pl.position.x == av.x && pl.position.y == av.y) {
					if (pl.roomOccupied.equals("none")) {
						available.remove(av);
						break;
					}
					if (pl.name.equals(p.name)) {
						available.remove(av);
						break;
					}
				}
			}
		}
		return available;
	}

	public static Position addAvailable(int d, int x, int y, ArrayList<Position> available, int oneOrZero) {
		int dx = x;
		int dy = y;
		switch (d) {
		case 0:
			dy = y - 1;
			break;
		case 1:
			dx = x + 1;
			break;
		case 2:
			dy = y + 1;
			break;
		case 3:
			dx = x - 1;
			break;
		}
		Position dp = new Position();
		dp.x = dx;
		dp.y = dy;
		for (Position ch : checked) {
			if (ch.x == dp.x && ch.y == dp.y) {
				dp.x = -1;
				dp.y = -1;
				return dp;
			}
		}
		checked.add(dp);
		boolean isValid = Board.isInGrid(dx, dy);
		if (isValid) {
			isValid = !(Board.board[dx][dy] == oneOrZero);
		}
		if (!isValid) {
			dp.x = -1;
			dp.y = -1;
			return dp;
		}
		dp.x = dx;
		dp.y = dy;
		isValid = checkDuplicate(dp, available, isValid);
		if (isValid) {
			return dp;
		} else {
			dp.x = -1;
			dp.y = -1;
			return dp;
		}

	}

	public static ArrayList<Position> checkMovedRoom(ArrayList<Position> available, Player p) {
		// TODO Auto-generated method stub
		int s = -1;
		Position r1 = new Position();
		Position r2 = new Position();
		Position r3 = new Position();
		Position r4 = new Position();
		for (int i = 0; i < EditRoom.roomNames.length; i++) {
			if (EditRoom.roomNames[i].equals(p.roomOccupied)) {
				s = i;
			}
		}
		switch (s) {
		case -1:
			break;
		case 0:
			r1.x = 20;
			r1.y = 4;
			break;
		case 1:
			r1.x = 20;
			r1.y = 7;
			r2.x = 17;
			r2.y = 9;
			r3.x = 17;
			r3.y = 10;
			break;
		case 2:
			r1.x = 18;
			r1.y = 15;
			break;
		case 3:
			r1.x = 15;
			r1.y = 5;
			r2.x = 13;
			r2.y = 2;
			break;
		case 4:
			r1.x = 8;
			r1.y = 4;
			break;
		case 5:
			r1.x = 3;
			r1.y = 4;
			break;
		case 6:
			r1.x = 6;
			r1.y = 9;
			r2.x = 6;
			r2.y = 10;
			r3.x = 6;
			r3.y = 11;
			r4.x = 4;
			r4.y = 13;
			break;
		case 7:
			r1.x = 5;
			r1.y = 17;
			break;
		case 8:
			r1.x = 11;
			r1.y = 14;
			r2.x = 14;
			r2.y = 18;
			break;
		}
		for (int i = 0; i < available.size(); i++) {
			if (available.get(i).x == r1.x && available.get(i).y == r1.y) {
				available.remove(i);
				i--;
			} else if (available.get(i).x == r2.x && available.get(i).y == r2.y) {
				available.remove(i);
				i--;
			} else if (available.get(i).x == r3.x && available.get(i).y == r3.y) {
				available.remove(i);
				i--;
			} else if (available.get(i).x == r4.x && available.get(i).y == r4.y) {
				available.remove(i);
				i--;
			}
		}
		return available;

	}

	private static boolean checkDuplicate(Position dp, ArrayList<Position> available, boolean same) {
		for (Position xy : available.toArray(new Position[available.size()])) {
			if (xy.equals(dp)) {
				return false;
			}
		}
		return same;
	}

	public static void secretPassage(Player p) {
		switch (p.roomOccupied) {
		case "Study":
			p.position.x = 5;
			p.position.y = 17;
			p.roomOccupied = "Kitchen";
			break;
		case "Kitchen":
			p.position.x = 20;
			p.position.y = 4;
			p.roomOccupied = "Study";
			break;
		case "Conservatory":
			p.position.x = 18;
			p.position.y = 15;
			p.roomOccupied = "Lounge";
			break;
		case "Lounge":
			p.position.x = 3;
			p.position.y = 4;
			p.roomOccupied = "Conservatory";
			break;
		}
		CharacterPawn.moveToAvailableSpaceInRoom(p);

	}

	public static void getMoveChoice(Player p) throws IOException {
		boolean validMove = false;
		boolean valid = false;
		int x = 0;
		int y = 0;
		do {
			do {
				System.out.println("Please enter the coordinates of the space you would like to move to.\n"
						+ "If you would like to print the board again, type 'board'.");
				String input = ConsoleUI.promptForInput("Input must be in the form of A:1", false);
				if (input.equalsIgnoreCase("board")) {
					Game.printScreen();
				}
				if (input.equalsIgnoreCase("notes")) {
					DetectivePad.padMenu(p);
				}
				if (input.length() > 2) {
					if (input.charAt(1) == ':') {
						if (input.charAt(0) >= 64 && input.charAt(0) <= 87) {
							x = input.charAt(0) - 65;
							if (input.substring(2, input.length()).length() <= 2) {
								if (input.charAt(2) >= 48 && input.charAt(2) <= 57) {
									if (input.charAt(input.length() - 1) >= 48
											&& input.charAt(input.length() - 1) <= 57) {
										String yString = input.substring(2, input.length());
										y = Integer.parseInt(yString);
										valid = true;
									}
								}
							}
						}

					}
				}

			} while (!valid);
			for (Position dp : Board.available) {
				if (dp.x == x && dp.y == y) {
					Position output = new Position();
					output.x = x;
					output.y = y;
					p.position = output;
					validMove = true;
					if (Board.board[p.position.x][p.position.y] == 2 || Board.board[p.position.x][p.position.y] == 3) {
						p.roomOccupied = Board.getRoom(p);
					} else if (Board.board[p.position.x][p.position.y] == 0) {
						p.roomOccupied = "none";
					}
				}
			}
			if (!validMove) {
				System.out.println("That is not a valid space to move. Try again.");
			}
		} while (!validMove);

	}

	public static Position moveToAvailableSpaceInRoom(Player p) {
		ArrayList<Position> available = new ArrayList<>();
		ArrayList<Position> unchecked = new ArrayList<>();
		checked = new ArrayList<>();
		for (int d = 0; d < 4; d++) {
			Position dp = addAvailable(d, p.position.x, p.position.y, available, 0);
			if (dp.x == p.position.x && dp.y == p.position.y) {

			} else {
				available.add(dp);
				unchecked.add(dp);
			}
		}
		for (int i = 0; i < 4 - 1; i++) {
			int u = unchecked.size();
			for (int j = 0; j < u; j++) {
				for (int d = 0; d < 4; d++) {
					Position dp = addAvailable(d, unchecked.get(0).x, unchecked.get(0).y, available, 0);
					if (dp.equals(p.position)) {

					} else if (dp.x == -1) {

					} else {
						available.add(dp);
						unchecked.add(dp);
					}
				}
				unchecked.remove(0);
			}
		}

		for (Player pl : Game.Players) {
			for (Position av : available) {
				if (pl.position.x == av.x && pl.position.y == av.y) {
					if (pl.roomOccupied.equals("none")) {
						available.remove(av);
						break;
					}
					if (pl.name.equals(p.name)) {
						available.remove(av);
						break;
					}
				}
			}
		}
		Random r = new Random();
		return available.get(r.nextInt(available.size()));
	}

	public static ArrayList<Position> getPossibleStartPositions(Player p) {
		ArrayList<Position> possible = new ArrayList<>();
		Position r1 = new Position();
		Position r2 = new Position();
		Position r3 = new Position();
		Position r4 = new Position();
		r1 = p.position;
		int s = -1;
		for (int i = 0; i < EditRoom.roomNames.length; i++) {
			if (EditRoom.roomNames[i].equals(p.roomOccupied)) {
				s = i;
			}
		}
		switch (s) {
		case 0:
			r1.x = 20;
			r1.y = 4;
			break;
		case 1:
			r1.x = 20;
			r1.y = 7;
			r2.x = 17;
			r2.y = 9;
			r3.x = 17;
			r3.y = 10;
			break;
		case 2:
			r1.x = 18;
			r1.y = 15;
			break;
		case 3:
			r1.x = 15;
			r1.y = 5;
			r2.x = 13;
			r2.y = 2;
			break;
		case 4:
			r1.x = 8;
			r1.y = 4;
			break;
		case 5:
			r1.x = 3;
			r1.y = 4;
			break;
		case 6:
			r1.x = 6;
			r1.y = 9;
			r2.x = 6;
			r2.y = 10;
			r3.x = 6;
			r3.y = 11;
			r4.x = 4;
			r4.y = 13;
			break;
		case 7:
			r1.x = 5;
			r1.y = 17;
			break;
		case 8:
			r1.x = 11;
			r1.y = 14;
			r2.x = 14;
			r2.y = 18;
			break;
		}
		possible.add(r1);
		possible.add(r2);
		possible.add(r3);
		possible.add(r4);
		for (int i = 0; i < possible.size(); i++) {
			if (possible.get(i).x == 0 && possible.get(i).y == 0) {
				possible.remove(i);
			}
		}
		return possible;
	}
}
