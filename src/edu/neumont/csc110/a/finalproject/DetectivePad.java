package edu.neumont.csc110.a.finalproject;

import java.io.IOException;

public class DetectivePad {
	static String[] Menu = { "Quit", "Jot down a note", "Read your notes" };
	static String[] subMenu = { "Quit", "Suspects", "Weapons", "Rooms" };
	static String[] suspects = { "Professor Plum", "Colonel Mustard", "Mr. Green", "Mrs. Peacock", "Miss Scarlet",
			"Mrs. White" };
	static String[] weapons = { "Candlestick", "Lead Pipe", "Knife", "Revolver", "Rope", "Wrench" };
	static String[] rooms = { "Study", "Hall", "Lounge", "Library", "Billiard Room", "Conservatory", "Ballroom",
			"Kitchen", "Dining Room" };

	public static int padMenu(Player p) throws IOException {
		// 1.Show Cards and which ones were crossed out.
		// 2.Prompt User to Write down a note, Look at notes, or to return to
		// the game.
	do {
		int selection = ConsoleUI.promptForMenuSelection(Menu, true);
		if (selection == 0) {
			return 0;
		} else if (selection == 1) {
			int subSelection = ConsoleUI.promptForMenuSelection(subMenu, true);
			if (subSelection == 1) {
				int input = ConsoleUI.promptForMenuSelection(suspects, true);
				String note = ConsoleUI.promptForInput(
						"Please jot down your note. This will be added to any existing notes you have.", true);
				p.myNotes.get(input).note = p.myNotes.get(input).note.concat(" " + note + ",");
				// Add to Note
			} else if (subSelection == 2) {
				int input = ConsoleUI.promptForMenuSelection(weapons, true) + 6;
				String note = ConsoleUI.promptForInput(
						"Please jot down your note. This will be added to any existing notes you have.", true);
				p.myNotes.get(input).note = p.myNotes.get(input).note
						.concat(" " + note + ",");
				// Add to Note
			} else if (subSelection == 3) {
				int input = ConsoleUI.promptForMenuSelection(rooms, true) + 12;
				String note = ConsoleUI.promptForInput(
						"Please jot down your note. This will be added to any existing notes you have.", true);
				p.myNotes.get(input).note = p.myNotes.get(input).note
						.concat(" " + note + ",");
				// Add to Note
			}
		} else if (selection == 2) {
			// Return notes
			printNotes(p);
		}
	} while(true);

	}

	public static void printNotes(Player p) {
		for (int i = 0; i < p.myNotes.size(); i++) {
			if(p.myNotes.get(i).isSeen){
				System.out.print("X ");
			}
			System.out.print(p.myNotes.get(i).name);
			System.out.println(p.myNotes.get(i).note);

		}
	}
	// Suspects
	// Weapons
	// Rooms
}
