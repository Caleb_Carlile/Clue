package edu.neumont.csc110.a.finalproject;

import java.io.IOException;

public class Clue {

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		// Main Class, Calls Games which calls other Classes
		do {
			Clue.menu();
			Game newGame = new Game();
			newGame.start();
		} while (true);
	}

	private static void menu() throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Welcome to the game of Clue!");
		int select = ConsoleUI.promptForMenuSelection(new String[]{"Quit", "New Game"}, true);
		if(select == 0) {
			System.exit(0);
		}
	}
}
