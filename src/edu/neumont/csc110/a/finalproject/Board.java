package edu.neumont.csc110.a.finalproject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Board {

	public static ArrayList<Position> available = new ArrayList<>();
	public static int[][] board = new int[23][22];
	private static int W = 55;
	private static int M = 50;
	public static JFrame window = new JFrame("Clue Board");

	public static void makeBoard() {
		board[0] = new int[] { 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1 };
		board[1] = new int[] { 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1 };
		board[2] = new int[] { 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1 };
		board[3] = new int[] { 1, 1, 1, 1, 2, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1 };
		board[4] = new int[] { 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 0, 0, 1, 1, 1, 1, 1, 1 };
		board[5] = new int[] { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 2, 1, 1, 1, 1 };
		board[6] = new int[] { 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 };
		board[7] = new int[] { 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		board[8] = new int[] { 1, 1, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1 };
		board[9] = new int[] { 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
		board[10] = new int[] { 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
		board[11] = new int[] { 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1 };
		board[12] = new int[] { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
		board[13] = new int[] { 1, 1, 2, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
		board[14] = new int[] { 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 2, 1, 1, 1 };
		board[15] = new int[] { 1, 1, 1, 1, 1, 2, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		board[16] = new int[] { 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		board[17] = new int[] { 1, 1, 1, 1, 1, 0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		board[18] = new int[] { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 2, 1, 1, 1, 1, 1, 1 };
		board[19] = new int[] { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
		board[20] = new int[] { 1, 1, 1, 1, 2, 1, 0, 2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
		board[21] = new int[] { 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
		board[22] = new int[] { 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 };
	}

	private static class Draw extends JPanel {

		private static final long serialVersionUID = -2400518497035750538L;

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			for (int i = 0; i < board.length; i++) {
				for (int j = 0; j < board[i].length; j++) {
					if (board[i][j] == 0) {
						g.setColor(new Color(220, 220, 220));
						g.fillRect(i * W + M, j * W + M, W, W);
						g.setColor(Color.black);
						g.drawRect(i * W + M, j * W + M, W, W);
					}
					if (board[i][j] == 1 || board[i][j] == 2 || board[i][j] == 3) {
						g.setColor(new Color(190, 190, 190));
						g.fillRect(i * W + M, j * W + M, W, W);
					}
				}
			}
			for (int i = 0; i < 23; i++) {
				g.setColor(Color.BLACK);
				g.setFont(new Font("Arial", Font.PLAIN, 24));
				g.drawString(Character.toString((char) (i + 65)), i * W + M + M / 2 - 5, 30);
			}
			for (int i = 0; i < 23; i++) {
				g.setColor(Color.BLACK);
				g.setFont(new Font("Arial", Font.PLAIN, 24));
				g.drawString(String.valueOf(i), 15, i * W + M + M - M / 3);
			}
			for (Position p : available) {
				if (p.x == -1) {
				} else {
					g.setColor(Color.ORANGE);
					g.fillRect(p.x * W + M, p.y * W + M, W, W);
					g.setColor(Color.BLACK);
					g.drawRect(p.x * W + M, p.y * W + M, W, W);
				}
			}
			g.setColor(Color.blue);
			g.fillRect(4 * W + M, 4 * W + M, W / 8, W);
			g.fillRect(6 * W + M, 17 * W + M, W / 8, W);
			g.fillRect(7 * W + M, 9 * W + M, W / 8, W);
			g.fillRect(7 * W + M, 10 * W + M, W / 8, W);
			g.fillRect(7 * W + M, 11 * W + M, W / 8, W);
			g.fillRect(15 * W + M, 18 * W + M, W / 8, W);
			g.fillRect(17 * W + M - W / 8, 9 * W + M, W / 8, W);
			g.fillRect(17 * W + M - W / 8, 10 * W + M, W / 8, W);
			g.fillRect(18 * W + M - W / 8, 15 * W + M, W / 8, W);
			g.fillRect(13 * W + M - W / 8, 2 * W + M, W / 8, W);
			g.fillRect(20 * W + M - W / 8, 4 * W + M, W / 8, W);
			g.fillRect(8 * W + M, 5 * W + M, W, W / 8);
			g.fillRect(15 * W + M, 6 * W + M, W, W / 8);
			g.fillRect(4 * W + M, 14 * W + M, W, W / 8);
			g.fillRect(20 * W + M, 7 * W + M - W / 8, W, W / 8);
			g.fillRect(11 * W + M, 14 * W + M - W / 8, W, W / 8);
			for (int i = 0; i < Game.playerCount; i++) {
				g.setColor(Game.Players.get(i).myColor);
				int x = Game.Players.get(i).position.x;
				int y = Game.Players.get(i).position.y;
				g.fillOval(x * W + M + 1, y * W + M + 1, W - 2, W - 2);
				g.setColor(Color.BLACK);
				g.drawOval(x * W + M + 1, y * W + M + 1, W - 2, W - 2);

			}
			g.setColor(Color.BLUE);
			g.setFont(new Font("Arial", Font.BOLD, 24));
			g.drawString("Conservatory", 2 * W, 3 * W);
			g.drawString("Billiard Room", 9 * W, 3 * W);
			g.drawString("Library", 15 * W + W / 2, 3 * W);
			g.drawString("Study", 22 * W, 3 * W);
			g.drawString("BallRoom", 4 * W, 11 * W);
			g.drawString("Hall", 20 * W + W / 2, 11 * W);
			g.drawString("Kitchen", 3 * W, 19 * W);
			g.drawString("Dining Room", 11 * W, 19 * W);
			g.drawString("Lounge", 20 * W + W / 2, 19 * W);
			/*
			 * for (int i = 0; i < board.length; i++) { g.setColor(Color.BLACK);
			 * g.drawRect(i * W + M, 0, W - 1, window.getHeight()); } for (int i
			 * = 0; i < board[0].length; i++) { g.setColor(Color.BLACK);
			 * g.drawRect(0, i * W + M, window.getWidth(), W - 1);
			 */
			for (Position av : available) {
				if (av.x == -1) {

				} else {
					String x = Character.toString((char) (av.x + 65));
					String y = String.valueOf(av.y);
					String coor = x.concat(":").concat(y);
					g.setColor(Color.BLACK);
					g.setFont(new Font("Arial", Font.BOLD, 22));
					g.drawString(coor, av.x * W + M + 5, av.y * W + M + W / 2 + 5);
				}
			}
		}
	}

	private static class ButtonHandler implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			window.dispose();
		}
	}

	public static void printBoard() {
		Draw displayPanel = new Draw();
		JButton okButton = new JButton("OK");
		ButtonHandler listener = new ButtonHandler();
		okButton.addActionListener(listener);

		JPanel content = new JPanel();
		content.setLayout(new BorderLayout());
		content.add(displayPanel, BorderLayout.CENTER);
		content.add(okButton, BorderLayout.SOUTH);

		window.setContentPane(content);
		window.setSize(board.length * W + M * 3, board[0].length * W + M + 100);
		window.setLocation(0, 0);
		window.setVisible(true);

	}

	public static boolean isInGrid(int x, int y) {
		if (x < 0 || y < 0)
			return false;
		if (x >= 23 || y >= 22)
			return false;
		return true;
	}

	public static void closeWindow() {
		window.dispose();
	}

	public static String getRoom(Player p) {
		for (int i = 0; i < 9; i++) {
			Position r1 = new Position();
			Position r2 = new Position();
			Position r3 = new Position();
			Position r4 = new Position();

			switch (i) {
			case 0:
				r1.x = 20;
				r1.y = 4;
				break;
			case 1:
				r1.x = 20;
				r1.y = 7;
				r2.x = 17;
				r2.y = 9;
				r3.x = 17;
				r3.y = 10;
				break;
			case 2:
				r1.x = 18;
				r1.y = 15;
				break;
			case 3:
				r1.x = 15;
				r1.y = 5;
				r2.x = 13;
				r2.y = 2;
				break;
			case 4:
				r1.x = 8;
				r1.y = 4;
				break;
			case 5:
				r1.x = 3;
				r1.y = 4;
				break;
			case 6:
				r1.x = 6;
				r1.y = 9;
				r2.x = 6;
				r2.y = 10;
				r3.x = 6;
				r3.y = 11;
				r4.x = 4;
				r4.y = 13;
				break;
			case 7:
				r1.x = 5;
				r1.y = 17;
				break;
			case 8:
				r1.x = 11;
				r1.y = 14;
				r2.x = 14;
				r2.y = 18;
				break;
			}
			String[] roomNames = new String[] { "Study", "Hall", "Lounge", "Library", "Billiard Room", "Conservatory",
					"Ballroom", "Kitchen", "Dining Room" };
			if (p.position.x == r1.x && p.position.y == r1.y) {
				return roomNames[i];
			}
			if (p.position.x == r2.x && p.position.y == r2.y) {
				return roomNames[i];
			}
			if (p.position.x == r3.x && p.position.y == r3.y) {
				return roomNames[i];
			}
			if (p.position.x == r4.x && p.position.y == r4.y) {
				return roomNames[i];
			}
		}
		return "none";
	}
}
