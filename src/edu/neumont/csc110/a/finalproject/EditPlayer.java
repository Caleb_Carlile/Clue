package edu.neumont.csc110.a.finalproject;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class EditPlayer {
	public static String[] detective = new String[] { "Professor Plum", "Colonel Mustard", "Mr. Green", "Mrs. Peacock",
			"Miss Scarlet", "Mrs. White" };
	public static String[] noteNames = new String[] { "Professor Plum", "Colonel Mustard", "Mr. Green", "Mrs. Peacock",
			"Miss Scarlet", "Mrs. White", "Candlestick", "Lead Pipe", "Knife", "Revolver", "Rope", "Wrench", "Study",
			"Hall", "Lounge", "Library", "Billiard Room", "Conservatory", "Ballroom", "Kitchen", "Dining Room" };
	public static ArrayList<String> available = new ArrayList<>();

	public static int getPlayerCount() throws IOException {
		return ConsoleUI.promptForInt("How many players?", 3, 6);
	}

	public static void playersJoined() throws IOException {
		Game.playerCount = getPlayerCount();
		for (int i = 0; i < 6; i++) {
			Game.Players.add(new Player());
			Game.Players.get(i).roomOccupied = "none";
		}
	}

	public static void initializeNotes() {
		for (int i = 0; i < Game.playerCount; i++) {
			for (int j = 0; j < 21; j++) {
				Game.Players.get(i).myNotes.add(new Note());
				Game.Players.get(i).myNotes.get(j).name = noteNames[j];
				Game.Players.get(i).myNotes.get(j).note = "";
			}
		}
	}

	public static void getPlayerName() throws IOException {
		for (int i = 0; i < detective.length; i++) {
			available.add(detective[i]);
		}
		for (int i = 1; i < Game.playerCount + 1; i++) {
			System.out.print("Player " + i + ", ");
			int j = ConsoleUI.promptForMenuSelection(detective, true);
			Game.Players.get(i - 1).name = detective[j];
			available.remove(j);
			detective = available.toArray(new String[available.size()]);
			Game.Players.get(i - 1).inGame = true;
		}
		for (int i = 0; i < 6 - Game.playerCount; i++) {
			Game.Players.get(i + Game.playerCount).name = detective[0];
			available.remove(0);
			detective = available.toArray(new String[available.size()]);
		}
	}

	public static void getMurderer() {
		Random rng = new Random();
		int i = rng.nextInt(6) + 1;
		int j = 0;
		for (Player murderIndex : Game.Players) {
			j++;
			if (j == i) {
				murderIndex.isKiller = true;
			}

		}
	}

	public static void setStartPosition() {
		for (int i = 0; i < Game.playerCount; i++) {
			Game.Players.get(i).roomOccupied = "none";
			switch (Game.Players.get(i).name) {
			case "Mrs. Peacock":
				Game.Players.get(i).position.x = 4;
				Game.Players.get(i).position.y = 0;
				Game.Players.get(i).myColor = Color.BLUE;
				break;
			case "Professor Plum":
				Game.Players.get(i).position.x = 19;
				Game.Players.get(i).position.y = 0;
				Game.Players.get(i).myColor = Color.MAGENTA;
				break;
			case "Mrs. White":
				Game.Players.get(i).position.x = 0;
				Game.Players.get(i).position.y = 12;
				Game.Players.get(i).myColor = Color.WHITE;
				break;
			case "Miss Scarlet":
				Game.Players.get(i).position.x = 22;
				Game.Players.get(i).position.y = 13;
				Game.Players.get(i).myColor = Color.RED;
				break;
			case "Mr. Green":
				Game.Players.get(i).position.x = 6;
				Game.Players.get(i).position.y = 21;
				Game.Players.get(i).myColor = Color.GREEN;
				break;
			case "Colonel Mustard":
				Game.Players.get(i).position.x = 15;
				Game.Players.get(i).position.y = 21;
				Game.Players.get(i).myColor = Color.YELLOW;
				break;

			}
		}

	}

	public static void addOptions() {
		for (int i = 0; i < Game.Players.size(); i++) {
			Game.Players.get(i).options.add("Do Nothing");
			Game.Players.get(i).options.add("Roll");
			Game.Players.get(i).options.add("Use a secret passage");
			Game.Players.get(i).options.add("Look at notes");
			Game.Players.get(i).options.add("Make an accusation");
		}
	}

	public static void initializeDetectives() {
		detective = new String[] { "Professor Plum", "Colonel Mustard", "Mr. Green", "Mrs. Peacock", "Miss Scarlet",
				"Mrs. White" };
	}
}
